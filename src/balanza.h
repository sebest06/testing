#ifndef BALANZA_H_
#define BALANZA_H_

#define ADC_HI_SPEED 80
#define ADC_LO_SPEED 10
	
/*!
 * Estructura para almacenar datos en el espacio de memoria de usuario
 */
typedef struct
{
	float factorCorreccion;				/**< Factor de correccion en porcentaje*/
	unsigned int capMax;				/**< Peso maximo que se puede pesar*/
	unsigned int totalCelda;			/**< peso maximo de la celda*/
	unsigned int sensibilidadCelda;		/**< censibilidad de la celda*/
	float divisiones;					/**< divisiones de trabajo*/
	unsigned int sizeVentanaMovil;		/**< tamanio de la ventana movil*/
	float kgFiltroMovimiento;			/**< kilos para usar el kiltro de movimiento*/
	unsigned int sizeConversiones;		/**< cantidad de conversiones para promediar*/
	unsigned int sizeRecortes;			/**< cantidad de recortes, se recorta el doble de este numero*/
	unsigned int timeEstabilidad;		/**< Tiempo para determinar la estabilidad*/
	unsigned char unidad;				/**< Unidad de trabajo*/
	float pesoCero;						/**< Peso que representa al cero*/
	char adcSpeed;
} userData;

/*!
 * Estructura para trabajar con los filtros
 */
typedef struct  
{
	unsigned int indiceVentana;			/**< indice en que se esta trabajando en la ventana movil del filtro*/
	long vectorFiltro[100];				/**< vector para el filtro de ventana movil */
	long pesoFiltrado;					/**< devuelve el peso filtrado */
	long priv_pesoFisicoAnterior;		/**< conserva el valor anterior, para calcular la estabilidad VER*/
	unsigned char priv_ventanaCargada;	/**< permite saber si ya esta cargada toda la ventana movil para dividir*/
}filtro_adc;

/*!
 * Estructura para trabajar con el peso
 */
typedef struct
{
	long pesoPromediado;				/**< guarda el peso ya promediado*/
	long vectorPesos[100];				/**< guarda las conversiones para promediar*/
	unsigned int priv_index_vector_promedios;	/**< indicie para el vector*/
	float multiplicador;				/**< faltor para multiplicar para obtener el peso fisico*/
	float pesoAnterior;					/**< guarda el peso anterior, para la estabilidad*/
	char pesoEstable;					/**< dice si el peso esta estable o no*/
	float pesoResetHacienda;			/**< peso para resetear, para trabajar en hacienda*/
}balanza_adc;

/*!
 * Estructura de timers calculo de estabilidad
 */
typedef struct
{
	char enableTimer;					/**< activa el timer*/
	unsigned int timer;					/**< cantidad de tiempo*/
}balanza_timer;

/*!
 * Enumeracion para determinar que tipo de filtro esta activo
 */
typedef enum {FILTROVENTANAMOVIL = 0, FILTROMOVIMIENTO, SINFILTRO};
	
/*!
 * Enumeracion para determinar con que tipo de unidad se esta trabajando.
 */
typedef enum {KILOGRAMOS = 0, LIBRAS, GRAMOS};	

void initBalanza(userData parametrosInit);

unsigned int recalcularSensibilidad(long adcCrudos, float pesoFisicoConocido);
float recalcularParametrosBalanza(void);

long converPesoToCuentas(float PesoFisico);
float redondear(long unPesoCrudo);
void updateDivisiones(float divisiones);


#endif /* BALANZA_H_ */
