#include "balanza.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

balanza_adc AdcDatos;
filtro_adc ElfiltroVentanaMovil;

balanza_timer timerEstabilidad;

float auxCero;
userData balanzaRam;

void initBalanza(userData parametrosInit)
{
	memcpy(&balanzaRam,&parametrosInit,sizeof(userData));
	recalcularParametrosBalanza();
}

unsigned int recalcularSensibilidad(long adcCrudos, float pesoFisicoConocido)
{
	if(balanzaRam.unidad == LIBRAS)
	{
		pesoFisicoConocido /= 2.2046;
	}
	else if(balanzaRam.unidad == GRAMOS)
	{
		pesoFisicoConocido /= 1000;
	}
	
	return (adcCrudos * 0.000000014901218 * balanzaRam.totalCelda)/(pesoFisicoConocido * 0.000001);
}

float recalcularParametrosBalanza(void)
{
	AdcDatos.multiplicador = balanzaRam.totalCelda * 0.000000014901218 / (balanzaRam.sensibilidadCelda*0.000001);
	if(balanzaRam.unidad == LIBRAS)
	{
		AdcDatos.multiplicador *= 2.2046;
	}
	else if(balanzaRam.unidad == GRAMOS)
	{
		AdcDatos.multiplicador *= 1000;
	}
	return AdcDatos.multiplicador;
}

long converPesoToCuentas(float PesoFisico)
{
	return (PesoFisico / AdcDatos.multiplicador);
}

float redondear(long unPesoCrudo)
{
	float crudo,redondeo,redondeoarr,redondeoaba,abajo,arriba;
	float redondo, pesocrudo;
	
	pesocrudo = unPesoCrudo * AdcDatos.multiplicador;
	pesocrudo = pesocrudo - balanzaRam.pesoCero; //Primero se restan las cuentas del cero y dsp se corrige
	
	
	redondeo = (int)(pesocrudo/balanzaRam.divisiones);		   
	redondeoaba = redondeo*balanzaRam.divisiones;
	redondeoarr = (redondeo + 1)*balanzaRam.divisiones;
	abajo = pesocrudo - redondeoaba;
	arriba = redondeoarr - pesocrudo;

	if (abajo < arriba)
	{
		pesocrudo=redondeoaba;
	}
	else if ( arriba < abajo)
	{
		pesocrudo=redondeoarr;
	}
	else
	{
		pesocrudo=redondeoaba;
	}
	return pesocrudo;
}

void updateDivisiones(float divisiones)
{
	balanzaRam.divisiones = divisiones;
}

