#include "unity.h"
#include "balanza.h"

void setUp(void)
{
	userData balanza;
	balanza.unidad = KILOGRAMOS;
	balanza.totalCelda = 4000;
	balanza.sensibilidadCelda = 3000;
	balanza.pesoCero = 0;
	balanza.divisiones = 1;
	
	initBalanza(balanza);
}

void tearDown(void)
{
}

void test_primer_test(void)
{
	TEST_ASSERT_EQUAL(1192,recalcularSensibilidad(2000,100.0));
	TEST_ASSERT_EQUAL(2999,recalcularSensibilidad(5033,100.0));		
}

void test_calcular_factor_multiplicativo(void)
{
	TEST_ASSERT_EQUAL_FLOAT(0.0198683,recalcularParametrosBalanza());
}

void test_calcular_conver_peso_a_cuentas(void)
{
	TEST_ASSERT_EQUAL(5033,converPesoToCuentas(100.0));
	TEST_ASSERT_EQUAL(0,converPesoToCuentas(0.0));
	TEST_ASSERT_EQUAL(10066,converPesoToCuentas(200.0));
}

void test_redondear()
{
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5010));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5020));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5030));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5033));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5040));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5050));

	updateDivisiones(10);
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5060));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5100));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5200));
	TEST_ASSERT_EQUAL_FLOAT(100.0,redondear(5250));
	TEST_ASSERT_EQUAL_FLOAT(110.0,redondear(5300));

}
