#include "build/temp/_test_cuentas.c"
#include "balanza.h"
#include "unity.h"


void setUp(void)

{

 userData balanza;

 balanza.unidad = KILOGRAMOS;

 balanza.totalCelda = 4000;

 balanza.sensibilidadCelda = 3000;

 balanza.pesoCero = 0;

 balanza.divisiones = 1;



 initBalanza(balanza);

}



void tearDown(void)

{

}



void test_primer_test(void)

{

 UnityAssertEqualNumber((UNITY_INT)((1192)), (UNITY_INT)((recalcularSensibilidad(2000,100.0))), (

((void *)0)

), (UNITY_UINT)(22), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((2999)), (UNITY_INT)((recalcularSensibilidad(5033,100.0))), (

((void *)0)

), (UNITY_UINT)(23), UNITY_DISPLAY_STYLE_INT);

}



void test_calcular_factor_multiplicativo(void)

{

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((0.0198683)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((0.0198683))), (UNITY_FLOAT)((UNITY_FLOAT)((recalcularParametrosBalanza()))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(28)));

}



void test_calcular_conver_peso_a_cuentas(void)

{

 UnityAssertEqualNumber((UNITY_INT)((5033)), (UNITY_INT)((converPesoToCuentas(100.0))), (

((void *)0)

), (UNITY_UINT)(33), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((0)), (UNITY_INT)((converPesoToCuentas(0.0))), (

((void *)0)

), (UNITY_UINT)(34), UNITY_DISPLAY_STYLE_INT);

 UnityAssertEqualNumber((UNITY_INT)((10066)), (UNITY_INT)((converPesoToCuentas(200.0))), (

((void *)0)

), (UNITY_UINT)(35), UNITY_DISPLAY_STYLE_INT);

}



void test_redondear()

{

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5010)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(40)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5020)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(41)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5030)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(42)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5033)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(43)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5040)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(44)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5050)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(45)));



 updateDivisiones(10);

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5060)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(48)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5100)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(49)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5200)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(50)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((100.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((100.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5250)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(51)));

 UnityAssertFloatsWithin((UNITY_FLOAT)((UNITY_FLOAT)((110.0)) * (UNITY_FLOAT)(0.00001f)), (UNITY_FLOAT)((UNITY_FLOAT)((110.0))), (UNITY_FLOAT)((UNITY_FLOAT)((redondear(5300)))), ((

((void *)0)

)), (UNITY_UINT)((UNITY_UINT)(52)));



}
